module.exports = {
  entry: {
    "client": "./client/Index.ts",
  },

  output: {
    filename: "[name]-bundle.js",
    path: __dirname + "/static"
  },

  devtool: "source-map",

  resolve: {
    alias: {
      src: __dirname + "/src"
    },
    extensions: [".ts", ".tsx", ".js", ".json"]
  },

  module: {
    rules: [
      { test: /\.tsx?$/, loader: "awesome-typescript-loader" },

      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
    ]
  },

  watchOptions: {
    poll: true
  }
};

