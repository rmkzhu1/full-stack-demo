import * as React from 'react';
import { Promise } from 'es6-promise';

interface Hotel {
    id: number;
    address: string;
    amenities: string[];
    hotel_name: string;
    image_url: string;
    num_reviews: number;
    num_stars: number;
    price: number;
}

interface State {
    city: string;
    checkin: string;
    checkout: string;
    snapTravelHotels: Hotel[];
    retailHotels: Hotel[];
}

export class Home extends React.Component<{}, State> {
    state: State = {
        city: undefined,
        checkin: undefined,
        checkout: undefined,
        snapTravelHotels: [],
        retailHotels: []
    };

    _renderInput(name: string, onChange: (s: string) => void) {
        return React.createElement('div', {},
            name, React.createElement('input', {
                onChange: e => onChange(e.target.value)
            })
        );
    }

    _onClickSubmit = () => {
        Promise.all<{hotels: Hotel[]}>(['snaptravel', 'retail'].map(provider => {
            return new Promise((resolve, reject) => {
                fetch('/get_data', {
                    method: 'POST',
                    body: JSON.stringify({
                        city : this.state.city,
                        checkin : this.state.checkin,
                        checkout : this.state.checkout,
                        provider : 'snaptravel'
                    }),
                    headers:{
                      'Content-Type': 'application/json'
                    }
                }).then(res => {
                    res.json().then(json => resolve(json))
                }).catch(e => reject(e));
            });
        })).then(v => {
            this.setState({snapTravelHotels: v[0].hotels, retailHotels: v[1].hotels})
        });
    }

    _renderHeader() {
        return React.createElement('tr', {style: {fontWeight: 'bold'}},
            React.createElement('td', {}, 'id'),
            React.createElement('td', {}, 'address'),
            React.createElement('td', {}, 'hotel name'),
            React.createElement('td', {}, 'reviews'),
            React.createElement('td', {}, 'stars'),
            React.createElement('td', {}, 'images'),
            React.createElement('td', {}, 'snaptravel price'),
            React.createElement('td', {}, 'retail price')
        );
    }

    _renderHotelRow(snapHotel: Hotel, retailHotel: Hotel) {
        return React.createElement('tr', {},
            React.createElement('td', {}, snapHotel.id),
            React.createElement('td', {}, snapHotel.address),
            React.createElement('td', {}, snapHotel.hotel_name),
            React.createElement('td', {}, snapHotel.num_reviews),
            React.createElement('td', {}, snapHotel.num_stars),
            React.createElement('td', {},
                React.createElement('a', {href: snapHotel.image_url}, 'Image')),
            React.createElement('td', {}, snapHotel.price),
            React.createElement('td', {}, retailHotel.price)
        );
    }

    _renderHotels() {
        return React.createElement('table', {},
            this._renderHeader(),
            ...this.state.snapTravelHotels.map(hotel => {
                let retailHotel;
                this.state.retailHotels.forEach(rHotel => {
                    if (rHotel.id === hotel.id) {
                        retailHotel = rHotel
                    }
                });
                return {
                    snapHotel: hotel,
                    retailHotel
                }
            }).filter(x => x.retailHotel).map(hotel => {
                return this._renderHotelRow(hotel.snapHotel, hotel.retailHotel);
            })
        );
    }

    render() {
        return React.createElement('div', {},
            this._renderInput('city', s => this.setState({city: s})),
            this._renderInput('checkin', s => this.setState({checkin: s})),
            this._renderInput('checkout', s => this.setState({checkout: s})),
            React.createElement('button', {onClick: this._onClickSubmit}, 'Submit'),
            this._renderHotels()
        );
    }
}

export const create = React.createFactory(Home);