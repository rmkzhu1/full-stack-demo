import * as React from 'react';
import * as Home from './Home';

import { render } from 'react-dom';

render(Home.create({}), document.getElementById('root'));
