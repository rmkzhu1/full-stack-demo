var express = require('express');
var request = require('request');
var bodyParser = require('body-parser')
var app = new express();

var port = 3008;
app.use(bodyParser());
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html')
})

const cache = {};

app.post('/get_data', function(req, res) {
  console.log(req.body);
  if (cache[JSON.stringify(req.body)]) {
    console.log('Loaded from cache');
    return res.json(cache[JSON.stringify(req.body)]);
  }
  request.post('https://experimentation.getsnaptravel.com/interview/hotels',
      { json: req.body },
      function (error, response, body) {
        cache[JSON.stringify(req.body)] = body;
        res.json(body);
      }
  );
});

app.use('/static', express.static('static'))

app.listen(port, function(error) {
  if (error) {
    console.error(error)
  } else {
    console.info('==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port)
  }
})